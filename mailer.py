# by Galkin Ivan 2018
# for use it - just run: python3 mailer.py
import os
from os.path import basename
import logging
import smtplib
from email.header import Header
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders

_DEBUG = 0

# MAIL SERVER CONFIGURATIONS
_USE_TLS = True
_MAIL_SERVER = 'smtp.gmail.com'
_MAIL_PORT_TLS = 587
_MAIL_PORT_SSL = 993
_MAIL_LOGIN = 'someemail'
_MAIL_PASSWORD = 'some pass'

# MAIL MESSAGE CONFIGURATIONS
_MESSAGE_SUBJECT = 'TEST SUBJECT'
_MAIL_ATTACH_FILE = 'attach.txt'        # This files are inside folder data!
_MAIL_CONTACTS_FILE = 'contacts.txt'
_MAIL_MESSAGE_FILE = 'textmessage.txt'

# Logging settings
logging.basicConfig(format='%(asctime)s > %(message)s', datefmt='%d.%m.%Y %H:%M:%S', filename='error.log', level=logging.DEBUG)
os.environ["NLS_LANG"] = "Russian.AL32UTF8"

# getting full path to files
attatch_file = os.path.join(os.getcwd(), "data", _MAIL_ATTACH_FILE)
contacts_file = os.path.join(os.getcwd(), "data", _MAIL_CONTACTS_FILE)
textmessage_file = os.path.join(os.getcwd(), "data", _MAIL_MESSAGE_FILE)

# check files
if not os.path.exists(attatch_file):
    logging.debug('Attach file {} not exists!'.format(attatch_file))
    exit(1)
if not os.path.exists(contacts_file):
    logging.debug('File with contacts {} not exists!'.format(contacts_file))
    exit(1)
if not os.path.exists(textmessage_file):
    logging.debug('Text body file {} not exists!'.format(textmessage_file))
    exit(1)

# start generating email message
msg = MIMEMultipart()
msg['From'] = _MAIL_LOGIN
msg['Subject'] = _MESSAGE_SUBJECT


with open(textmessage_file) as fp:
     msg.attach(MIMEText(fp.read()))

with open(contacts_file) as fp:
    contacts = fp.read()
    msg['To'] = contacts

# start generating attach file
part = MIMEBase('application', "octet-stream")
part.set_payload(open(attatch_file, 'rb').read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', 'attachment', filename=(Header(basename(attatch_file), 'utf-8').encode()))
msg.attach(part)

# start server login \ connection
try:
    if _USE_TLS:
        s = smtplib.SMTP(_MAIL_SERVER, _MAIL_PORT_TLS)
        s.starttls()
    else:
        s = smtplib.SMTP_SSL(_MAIL_SERVER, _MAIL_PORT_SSL)
    if _DEBUG > 0:
        s.debuglevel = _DEBUG
    s.ehlo()
except smtplib.SMTPException as e:
    logging.debug("Can't connect  to smtp server! {}".format(str(e)))
    exit(1)

try:
    s.login(_MAIL_LOGIN, _MAIL_PASSWORD)
except smtplib.SMTPException as e:
    logging.debug("Can't login into smtp server {}".format(str(e)))
    exit(1)

try:
    s.sendmail(_MAIL_LOGIN, contacts.split(','), msg.as_string())
    s.quit()
except smtplib.SMTPException as e:
    logging.debug("Can't send this message via SMTP. {}".format(str(e)))

exit(0)
